import { IPlayerDetails } from "./IPlayerDetails";

/**
 * @interface ICardDetail
 * @param id [required] - represents player's id
 * @param play [required] - represents player's details
 */

export interface ICardDetail {
    id: number;
    player: IPlayerDetails;
}