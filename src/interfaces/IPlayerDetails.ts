/**
 * @interface IPlayerDetails
 * @param firstname [required] - represents player's first name
 * @param lastname [required] - represents player's last name
 * @param birthday [required] - represents player's date of birth
 */

export interface IPlayerDetails {
    firstname: string;
    lastname: string;
    birthday: string
}