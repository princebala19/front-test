import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { BORN_ON, CREATE, DATE_FORMAT } from '../constants';
import { ICardDetail } from '../interfaces/ICardDetail';

import { fetchCollection } from '../lib/collection';

import './Collection.css';

export const Collection = () => {
  const [cardsData, setCardsData] = useState<Array<ICardDetail>>([]);
  const history = useHistory();

  useEffect(() => {
    fetchCollection().then((result) => setCardsData(result));
  }, []);

  /**
   * Step 1: Render the card
   */
  return (
    <React.Fragment>
      <div className="collection-container">
        {cardsData.map((cardsDetail: ICardDetail) => (
          <div className="card" key={cardsDetail.id}>
            <div className="container">
              <h4>{`${cardsDetail.player.firstname} ${cardsDetail.player.lastname}`}</h4>
              <p>
                {`${BORN_ON}${moment(cardsDetail.player.birthday).format(
                  DATE_FORMAT
                )}`}
              </p>
            </div>
          </div>
        ))}
      </div>
      <button onClick={() => history.push('/create-card')}>{CREATE}</button>
    </React.Fragment>
  );
};
