import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import {
  apiUrl,
  CARD_CREATION_ERROR_MESSAGE,
  CREATE,
  DATE_OF_BIRTH,
  DATE_OF_BIRTH_PLACEHOLDER,
  DATE_TYPE,
  FIRST_NAME,
  FIRST_NAME_PLACEHOLDER,
  LAST_NAME,
  LAST_NAME_PLACEHOLDER,
  TEXT_TYPE,
} from '../constants';
import { ICardDetail } from '../interfaces';

import './CreateCard.css';

/**
 * Step 3: Render a form and everything needed to be able to create a card
 */
export const CreateCard = () => {
  const [userFirstName, setUserFirstName] = useState<string>('');
  const [userlastName, setUserLastName] = useState<string>('');
  const [userDateOfBirth, setUserDateOfBirth] = useState<string>('');
  const [cardSaveFailed, setCardSaveFailed] = useState<boolean>(false);
  const history = useHistory();

  const handleFirstNameChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setUserFirstName(event.target.value);
  };

  const handleLastNameChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setUserLastName(event.target.value);
  };

  const handleDateChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setUserDateOfBirth(event.target.value);
  };

  const handleCreateCard = (): void => {
    if (!!userDateOfBirth && !!userFirstName && !!userlastName) {
      const cardPayload: ICardDetail = {
        id: Date.now(),
        player: {
          firstname: userFirstName,
          lastname: userlastName,
          birthday: moment(userDateOfBirth).format(),
        },
      };
      fetch(apiUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(cardPayload),
      })
        .then((response) => response.json())
        .then((data) => {
          if (data) {
            setCardSaveFailed(false);
            history.push('/collection');
          }
        })
        .catch(() => {
          setCardSaveFailed(true);
        });
    }
  };

  return (
    <div className="create-card-container">
      <div className="input-container">
        <label className="label">{FIRST_NAME}</label>
        <input
          type={TEXT_TYPE}
          placeholder={FIRST_NAME_PLACEHOLDER}
          value={userFirstName}
          onChange={handleFirstNameChange}
        />
      </div>
      <div className="input-container">
        <label className="label">{LAST_NAME}</label>
        <input
          type={TEXT_TYPE}
          placeholder={LAST_NAME_PLACEHOLDER}
          value={userlastName}
          onChange={handleLastNameChange}
        />
      </div>
      <div className="input-container">
        <label className="label">{DATE_OF_BIRTH}</label>
        <input
          type={DATE_TYPE}
          placeholder={DATE_OF_BIRTH_PLACEHOLDER}
          onChange={handleDateChange}
          value={userDateOfBirth}
        />
      </div>
      <button onClick={handleCreateCard}>{CREATE}</button>
      <div className="error">
        {cardSaveFailed ? CARD_CREATION_ERROR_MESSAGE : ''}
      </div>
    </div>
  );
};
