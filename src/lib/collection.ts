import { apiUrl } from "../constants";
import { ICardDetail } from "../interfaces/ICardDetail";

export async function fetchCollection() {
  /**
  * Step 2: Instead of directly returning the collection, fetch it from http://localhost:8001/cards
  */
  let cardsData: Array<ICardDetail>;
  await fetch(apiUrl)
    .then((response) => response.json())
    .then((cards) =>
      cardsData = cards
    )
    .catch((error) => {
      //console.error('Error:', error);
    });
  return cardsData
}
